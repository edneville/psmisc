# Japanese messages for psmisc.
# Copyright (C) 2006 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Goto, Masanori <gotom@debian.or.jp>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: psmisc 22.2pre1\n"
"Report-Msgid-Bugs-To: csmall@small.dropbear.id.au\n"
"POT-Creation-Date: 2006-02-27 18:12+1100\n"
"PO-Revision-Date: 2006-03-20 10:31+0900\n"
"Last-Translator: GOTO Masanori <gotom@debian.or.jp>\n"
"Language-Team: Japanese <translation-team-ja@lists.sourceforge.net>\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=EUC-JP\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/killall.c:73
#, c-format
msgid "Kill %s(%s%d) ? (y/N) "
msgstr "%s(%s%d) をkillしますか? (y/N)"

#: src/killall.c:116
#, c-format
msgid "Cannot get UID from process status\n"
msgstr "プロセスの現在状態からUIDを取得できませんでした\n"

#: src/killall.c:142 src/killall.c:664
#, c-format
msgid "Bad regular expression: %s\n"
msgstr "不正な正規表現: %s\n"

#: src/killall.c:338
#, c-format
msgid "skipping partial match %s(%d)\n"
msgstr "部分一致をスキップ %s(%d)\n"

#: src/killall.c:427
#, c-format
msgid "Killed %s(%s%d) with signal %d\n"
msgstr "%s(%s%d) をシグナル %d でkillしました\n"

#: src/killall.c:441
#, c-format
msgid "%s: no process killed\n"
msgstr "%s: どのプロセスもkillされませんでした\n"

#: src/killall.c:479
#, c-format
msgid ""
"Usage: pidof [ -eg ] NAME...\n"
"       pidof -V\n"
"\n"
"    -e      require exact match for very long names;\n"
"            skip if the command line is unavailable\n"
"    -g      show process group ID instead of process ID\n"
"    -V      display version information\n"
"\n"
msgstr ""
"使用法: pidof [ -eg ] プロセス名...\n"
"        pidof -V\n"
"\n"
"    -e      とても長いプロセス名に対して完全一致を要求する\n"
"            コマンドラインが利用できない場合は該当プロセスはスキップされる\n"
"    -g      プロセスIDの代りにプロセスグループIDを表示する\n"
"    -V      バージョン情報を表示する\n"
"\n"

#: src/killall.c:493
#, c-format
msgid "Usage: killall [-Z CONTEXT] [-u USER] [ -eIgiqrvw ] [ -SIGNAL ] NAME...\n"
msgstr "使用法: killall [-Z CONTEXT] [-u ユーザー] [ -eIgiqrvw ] [ -シグナル ] 名前...\n"

#: src/killall.c:496
#, c-format
msgid "Usage: killall [OPTION]... [--] NAME...\n"
msgstr "使用法: killall [オプション]... [--] 名前...\n"

#: src/killall.c:499
#, c-format
msgid ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          require exact match for very long names\n"
"  -I,--ignore-case    case insensitive process name match\n"
"  -g,--process-group  kill process group instead of process\n"
"  -i,--interactive    ask for confirmation before killing\n"
"  -l,--list           list all known signal names\n"
"  -q,--quiet          don't print complaints\n"
"  -r,--regexp         interpret NAME as an extended regular expression\n"
"  -s,--signal SIGNAL  send this signal instead of SIGTERM\n"
"  -u,--user USER      kill only process(es) running as USER\n"
"  -v,--verbose        report if the signal was successfully sent\n"
"  -V,--version        display version information\n"
"  -w,--wait           wait for processes to die\n"
msgstr ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact            とても長い名前に対して完全一致を要求する\n"
"  -I,--ignore-case      プロセス名の一致は大文字の小文字区別なく行う\n"
"  -g,--process-group    プロセスの代りにプロセスグループにシグナルを送る\n"
"  -i,--interactive      kill する前に確認を求める\n"
"  -l,--list             既知のシグナル名をすべて表示する\n"
"  -q,--quiet            kill するプロセスがない場合に表示を行わない\n"
"  -r,--regexp           指定した名前を拡張正規表現として解釈する\n"
"  -s,--signal シグナル  SIGTERMの代りに指定したシグナルを送る\n"
"  -u,--user ユーザ      指定したユーザで走行するプロセスのみkillする\n"
"  -v,--verbose          シグナルの送信に成功したら報告する\n"
"  -V,--version          バージョン情報を表示する\n"
"  -w,--wait             killしたプロセスが終了するまで待つ\n"

#: src/killall.c:515
#, c-format
msgid ""
"  -Z,--context REGEXP kill only process(es) having context\n"
"                      (must precede other arguments)\n"
msgstr ""
"  -Z,--context 正規表現 コンテキストを持つプロセスのみkillする\n"
"                        (他の引数よりも前に指定されていなければならない)\n"

#: src/killall.c:536 src/fuser.c:111 src/pstree.c:780
#, c-format
msgid ""
"Copyright (C) 1993-2005 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2005 Werner Almesberger and Craig Small\n"
"\n"

#: src/killall.c:538 src/fuser.c:113 src/pstree.c:782
#, c-format
msgid ""
"PSmisc comes with ABSOLUTELY NO WARRANTY.\n"
"This is free software, and you are welcome to redistribute it under\n"
"the terms of the GNU General Public License.\n"
"For more information about these matters, see the files named COPYING.\n"
msgstr ""
"PSmiscは完全に無保証です\n"
"これはフリーソフトウェアです。GNU一般公衆利用許諾契約書の条項に基づいて\n"
"再配布を自由に行ってかまいません。\n"
"これらについてより詳細な情報はCOPYINGファイルをご覧下さい。\n"

#: src/killall.c:638
#, c-format
msgid "Cannot find user %s\n"
msgstr "ユーザ %s を発見できません\n"

#: src/killall.c:697
#, c-format
msgid "Maximum number of names is %d\n"
msgstr "名前の最大指定可能数は %d\n"

#: src/killall.c:702 src/pstree.c:710
#, c-format
msgid "%s is empty (not mounted ?)\n"
msgstr "%s は空ディレクトリ (マウントされていない?)\n"

#: src/fuser.c:80
#, c-format
msgid ""
"Usage: fuser [ -a | -s | -c ] [ -n SPACE ] [ -SIGNAL ] [ -kimuv ] NAME...\n"
"             [ - ] [ -n SPACE ] [ -SIGNAL ] [ -kimuv ] NAME...\n"
"       fuser -l\n"
"       fuser -V\n"
"Show which processes use the named files, sockets, or filesystems.\n"
"\n"
"    -a        display unused files too\n"
"    -c        mounted FS\n"
"    -f        silently ignored (for POSIX compatibility)\n"
"    -i        ask before killing (ignored without -k)\n"
"    -k        kill processes accessing the named file\n"
"    -l        list available signal names\n"
"    -m        show all processes using the named filesystems\n"
"    -n SPACE  search in this name space (file, udp, or tcp)\n"
"    -s        silent operation\n"
"    -SIGNAL   send this signal instead of SIGKILL\n"
"    -u        display user IDs\n"
"    -v        verbose output\n"
"    -V        display version information\n"
"    -4        search IPv4 sockets only\n"
"    -6        search IPv6 sockets only\n"
"    -         reset options\n"
"\n"
"  udp/tcp names: [local_port][,[rmt_host][,[rmt_port]]]\n"
"\n"
msgstr ""
"使用法: fuser [ -a | -s | -c ] [ -n SPACE ] [ -シグナル ] [ -kimuv ] 名前...\n"
"              [ - ] [ -n SPACE ] [ -シグナル ] [ -kimuv ] 名前...\n"
"        fuser -l\n"
"        fuser -V\n"
"ファイル名・ソケット・ファイルシステムをどのプロセスが使用しているかを表示\n"
"\n"
"    -a        プロセスから参照されていないファイルも表示対象とする\n"
"    -c        -mオプションに同じ (POSIX互換性のためだけに存在)\n"
"    -f        つけても無視される (POSIX互換性のためだけに存在)\n"
"    -i        killする前に確認を求める (-kオプションをつけないときは無視される)\n"
"    -k        指定したファイルにアクセスしているプロセスをkillする\n"
"    -l        利用可能なシグナル名をすべて表示する\n"
"    -m        指定ファイル上のファイルシステムを使用する全プロセスを表示する\n"
"    -n SPACE  名前空間としてSPACE(file, udp, または tcp のどれか)を指定する \n"
"    -s        表示を抑制する\n"
"    -シグナル SIGKILLの代りに指定したシグナルを送る\n"
"    -u        ユーザIDを表示する\n"
"    -v        冗長表示を行う\n"
"    -V        バージョン情報を表示する\n"
"    -4        IPv4ソケットのみ検索する\n"
"    -6        IPv6ソケットのみ検索する\n"
"    -         リセットオプション\n"
"\n"
"  udp/tcp 名前: [ローカルポート][,[リモートホスト][,[リモートポート]]]\n"
"\n"

#: src/fuser.c:109
#, c-format
msgid "fuser (PSmisc) %s\n"
msgstr "fuser (PSmisc) %s\n"

#: src/fuser.c:136
#, c-format
msgid "Cannot open /proc directory: %s\n"
msgstr "/proc ディレクトリを開けません: %s\n"

#: src/fuser.c:272
#, c-format
msgid "Cannot allocate memory for matched proc: %s\n"
msgstr "一致したプロセスに対するメモリが獲得できません: %s\n"

#: src/fuser.c:305
#, c-format
msgid "Cannot stat mount point %s: %s\n"
msgstr "マウントポイント %s をstatできません: %s\n"

#: src/fuser.c:322 src/fuser.c:341 src/fuser.c:364
#, c-format
msgid "Cannot stat %s: %s\n"
msgstr "%s をstatできません: %s\n"

#: src/fuser.c:463
#, c-format
msgid "Cannot resolve local port %s: %s\n"
msgstr "ローカルポート %s を解決できません: %s\n"

#: src/fuser.c:479
#, c-format
msgid "Unknown local port AF %d\n"
msgstr "未知のローカルポート AF %d\n"

#: src/fuser.c:532 src/fuser.c:584
#, c-format
msgid "Cannot open protocol file \"%s\": %s\n"
msgstr "プロトコルファイル \"%s\" が開けません: %s\n"

#: src/fuser.c:705
msgid "Namespace option requires an argument."
msgstr "名前空間オプションは引数が必要です"

#: src/fuser.c:714
msgid "Invalid namespace name"
msgstr "不正な名前空間名"

#: src/fuser.c:770
msgid "You can only use files with mountpoint option"
msgstr "マウントポイントオプション(-mまたは-c)と一緒に使えるのはfile名前空間のみです"

#: src/fuser.c:799
msgid "No process specification given"
msgstr "引数が与えられていません"

#: src/fuser.c:804
msgid "You cannot use the mounted and mountpoint flags together"
msgstr "-mオプションと-cオプションは同時には使えません"

#: src/fuser.c:811
msgid "all option cannot be used with silent option."
msgstr "-aオプションは-sオプションと一緒には使用できません"

#: src/fuser.c:815
msgid "You cannot search for only IPv4 and only IPv6 sockets at the same time"
msgstr "-4オプションと-6オプションは同時に指定できません"

#: src/fuser.c:855
#, c-format
msgid ""
"\n"
"%*s USER        PID ACCESS COMMAND\n"
msgstr ""
"\n"
"%*s ユーザ     PID アクセス コマンド\n"

#: src/fuser.c:880 src/fuser.c:905
msgid "(unknown)"
msgstr "(不明)"

#: src/fuser.c:967
#, c-format
msgid "Cannot stat file %s: %s\n"
msgstr "ファイルをstatできません %s: %s\n"

#: src/fuser.c:1061
#, c-format
msgid "Cannot open /proc/net/unix: %s\n"
msgstr "/proc/net/unix が開けません: %s\n"

#: src/fuser.c:1097
#, c-format
msgid "Cannot open /etc/mtab: %s\n"
msgstr "/etc/mtab が開けません: %s\n"

#: src/fuser.c:1146
#, c-format
msgid "Kill process %d ? (y/N) "
msgstr "プロセス %d をkillしますか? (y/N) "

#: src/fuser.c:1170
#, c-format
msgid "Could not kill process %d: %s\n"
msgstr "プロセス %d を kill できません: %s\n"

#: src/fuser.c:1185
#, c-format
msgid "Cannot open a network socket.\n"
msgstr "ネットワークソケットが開けません.\n"

#: src/fuser.c:1189
#, c-format
msgid "Cannot find socket's device number.\n"
msgstr "ソケットのデバイス番号が見つかりません.\n"

#: src/pstree.c:374
#, c-format
msgid "Internal error: MAX_DEPTH not big enough.\n"
msgstr "内部エラー: MAX_DEPTH が十分大きくありません.\n"

#: src/pstree.c:750
#, c-format
msgid ""
"Usage: pstree [ -a ] [ -c ] [ -h | -H PID ] [ -l ] [ -n ] [ -p ] [ -u ]\n"
"              [ -A | -G | -U ] [ PID | USER ]\n"
"       pstree -V\n"
"Display a tree of processes.\n"
"\n"
"    -a     show command line arguments\n"
"    -A     use ASCII line drawing characters\n"
"    -c     don't compact identical subtrees\n"
"    -h     highlight current process and its ancestors\n"
"    -H PID highlight this process and its ancestors\n"
"    -G     use VT100 line drawing characters\n"
"    -l     don't truncate long lines\n"
"    -n     sort output by PID\n"
"    -p     show PIDs; implies -c\n"
"    -u     show uid transitions\n"
"    -U     use UTF-8 (Unicode) line drawing characters\n"
"    -V     display version information\n"
msgstr ""
"使用法: pstree [ -a ] [ -c ] [ -h | -H PID ] [ -l ] [ -n ] [ -p ] [ -u ]\n"
"              [ -A | -G | -U ] [ PID | ユーザ名 ]\n"
"        pstree -V\n"
"プロセスツリーを表示する。\n"
"\n"
"    -a     コマンドライン引数も表示\n"
"    -A     ASCII 文字を罫線表示に使用\n"
"    -c     同じ内容のサブツリーをまとめて短く表示しない\n"
"    -h     現在のプロセスとその先祖のプロセスを強調表示する\n"
"    -H PID 指定プロセスPIDとその先祖のプロセスを強調表示する\n"
"    -G     VT100 罫線文字を表示に使用\n"
"    -l     長い行を表示し、途中で打ち切りません\n"
"    -n     PIDでソートして表示\n"
"    -p     PIDも表示; -cオプションをつける効果を含みます\n"
"    -u     UIDの遷移状況を表示する\n"
"    -U     UTF-8 (Unicode) 文字を罫線表示に使用\n"
"    -V     バージョン情報を表示\n"

#: src/pstree.c:768
#, c-format
msgid "    -Z     show SELinux security contexts\n"
msgstr "    -Z     SELinuxセキュリティコンテキストを表示する\n"

#: src/pstree.c:771
#, c-format
msgid ""
"    PID    start at this PID; default is 1 (init)\n"
"    USER   show only trees rooted at processes of this user\n"
"\n"
msgstr ""
"    PID      指定したPIDから表示開始します; デフォルトは 1 (init)\n"
"    ユーザ名 指定したユーザとして動作するプロセスから派生するツリーのみ表示\n"
"\n"

#: src/pstree.c:778
#, c-format
msgid "pstree (PSmisc) %s\n"
msgstr "pstree (PSmisc) %s\n"

#: src/pstree.c:871
#, c-format
msgid "TERM is not set\n"
msgstr "TERMが設定されていません\n"

#: src/pstree.c:876
#, c-format
msgid "Can't get terminal capabilities\n"
msgstr "端末の機能を取得できませんでした\n"

#: src/pstree.c:920
#, c-format
msgid "No such user name: %s\n"
msgstr "指定ユーザ名は存在しません: %s\n"

#: src/pstree.c:936
#, c-format
msgid "No processes found.\n"
msgstr "プロセスは見つかりませんでした\n"

#: src/pstree.c:941
#, c-format
msgid "Press return to close\n"
msgstr "リターンキーを押すと閉じます\n"

#: src/signals.c:84
#, c-format
msgid "%s: unknown signal; %s -l lists signals.\n"
msgstr "%s: 未知のシグナル; %s -l によってシグナルを一覧表示します\n"

#~ msgid "Cannot open protocol file \"%s\": %s"
#~ msgstr "プロトコルファイル \"%s\" が開けません: %s"
